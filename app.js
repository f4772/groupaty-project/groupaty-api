const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

const app = express();

app.use(bodyParser.json());
app.use(cors());

const usersRoute = require('./routes/users');
const postsRoute = require('./routes/posts');
const groupssRoute = require('./routes/groups');

app.use('/users', usersRoute);
app.use('/posts', postsRoute);
app.use('/groups', groupssRoute);

//Connect to DB
mongoose.connect(process.env.DB_CONNECTION, () => {
    console.log("Connected to db");
})

const PORT = process.env.PORT || 5000;
app.listen(PORT);