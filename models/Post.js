const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    post: {
        type: String,
        required: true,
    },
    user_id: {
        type: String,
        required: true,
    },
    group_id: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    user_name: {
        type: String,
        required: true,
    },
    group_name: {
        type: String,
        required: true,
    }
});

module.exports = mongoose.model('Posts', PostSchema);