const express = require('express');
const User = require('../models/User');
const router = express.Router();
const Post = require('../models/Post');
const Group = require('../models/Group');

//Get all users
router.get('/', async(req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (err) {
        res.json({ message: err });
    }
});

//Get a single user
router.get('/:id', async(req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.json(user);
    } catch (err) {
        res.json({ message: err });
    }
});

//Add a new user
router.post('/register', async(req, res) => {
    // console.log(req.body);
    const finduser = await User.find({ "email": req.body.email }).select();

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        bio: req.body.bio
    });
    try {
        if (finduser.length == 0) {
            const newUser = await user.save();
            res.json(newUser);
        } else {
            res.status(400).json("User with this email already exists");
        }
    } catch (err) {
        res.json({ message: err });
    }
});

//Delete a user
router.delete('/:id', async(req, res) => {
    try {
        await User.deleteOne({ '_id': req.params.id });
        const users = await User.find();
        res.json(users);
    } catch (err) {
        res.json({ message: err });
    }
});

//Update a user
router.patch('/:id', async(req, res) => {
    try {
        const user = await User.findById(req.params.id);
        const updatedUser = await User.updateOne({ _id: req.params.id }, {
            $set: {
                name: req.body.name ? req.body.name : user.name,
                email: req.body.email ? req.body.email : user.email,
                password: req.body.password ? req.body.password : user.password,
                bio: req.body.bio ? req.body.bio : user.bio,
            }
        });
        res.json(updatedUser);
    } catch (err) {
        res.json({ message: err });
    }
});

//Get user posts
router.get('/:id/posts', async(req, res) => {
    try {
        const posts = await Post.find({ "user_id": req.params.id }).select();
        res.json(posts);
    } catch (err) {
        res.json({ message: err });
    }
});

//Login with email&password
router.post('/login', async(req, res) => {
    try {
        const user = await User.find({ "email": req.body.email, "password": req.body.password }).select();
        if (user.length >= 1) {
            res.json(user[0]);
        } else {
            res.status(400).json({ msg: "Can't find user" });
        }

    } catch (err) {
        res.json({ message: err });
    }
});

//Get newsfeed posts
router.get('/:id/newsfeed', async(req, res) => {
    try {
        var itemsProcessed = 0;
        var arr = [];
        const user = await User.findById(req.params.id);
        user.groups.forEach(async(item) => {
            const posts = await Post.find({ "group_id": item.group_id }).select();
            itemsProcessed++;
            console.log(posts.length);
            arr.push.apply(arr, posts);
            if (itemsProcessed == user.groups.length) {
                res.json(arr);
            }
        });
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;