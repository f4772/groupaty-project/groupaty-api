const express = require('express');
const Group = require('../models/Group');
const router = express.Router();
const Post = require('../models/Post');
const User = require('../models/User');

//Get all groups
router.get('/', async(req, res) => {
    try {
        const groups = await Group.find();
        res.json(groups);
    } catch (err) {
        res.json({ message: err });
    }
});

//Get a single group
router.get('/:id', async(req, res) => {
    try {
        const group = await Group.findById(req.params.id);
        res.json(group);
    } catch (err) {
        res.json({ message: err });
    }
});

//Add a new group
router.post('/', async(req, res) => {
    const group = new Group({
        name: req.body.name,
    });
    try {
        const newGroup = await group.save();
        res.json(newGroup);
    } catch (err) {
        res.json({ message: err });
    }
});

//Get group posts
router.get('/:id/posts', async(req, res) => {
    try {
        const posts = await Post.find({ "group_id": req.params.id }).select();
        res.json(posts);
    } catch (err) {
        res.json({ message: err });
    }
});

//Add member to group
router.patch('/:id/join', async(req, res) => {
    try {
        const user = await User.findById(req.params.id).select();
        var group = { group_id: req.body.group_id };
        user.groups.push(group);
        user.save();
        res.send("User added to group");
    } catch (err) {
        res.json({ message: err });
    }
});

//Remove member from group
router.patch('/:id/leave', async(req, res) => {
    try {
        const user = await User.findById(req.params.id).select();
        const removedGroup = await Group.findById(req.body.group_id).select();
        user.groups = user.groups.filter(function(item) {
            return item.group_id !== removedGroup.id;
        });
        user.save();
        res.send("User left a group");
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;