const express = require('express');
const Post = require('../models/Post');
const router = express.Router();
const moment = require('moment');
const User = require('../models/User');
const Group = require('../models/Group');

//Get all posts
router.get('/', async(req, res) => {
    try {
        const posts = await Post.find();
        res.json(posts);
    } catch (err) {
        res.json({ message: err });
    }
});

//Get a single post
router.get('/:id', async(req, res) => {
    try {
        const post = await Post.findById(req.params.id);
        res.json(post);
    } catch (err) {
        res.json({ message: err });
    }
});

//Add a new post
router.post('/', async(req, res) => {
    console.log(req.body);
    const user = await User.findById(req.body.user_id);
    const group = await Group.findById(req.body.group_id);
    const post = new Post({
        post: req.body.post,
        user_id: req.body.user_id,
        group_id: req.body.group_id,
        date: moment().format('yyyy-MM-DD hh:mm:ss'),
        user_name: user.name,
        group_name: group.name,
    });
    try {
        const newPost = await post.save();
        res.json(newPost);
    } catch (err) {
        res.json({ message: err });
    }
});

//Delete a post
router.delete('/:id', async(req, res) => {
    try {
        await Post.deleteOne({ '_id': req.params.id });
        const posts = await Post.find();
        res.json(posts);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;